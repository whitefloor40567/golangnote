package main

import (
	"fmt"
)

type student struct {
	Name string
	Age  int
}

// map value的地方用上pointer會導致value全部指向最後的輸出結果
// 解決方法：不要用pointer

func paseStudent() map[string]*student {
	m := make(map[string]*student)
	stus := []student{
		{Name: "zhou", Age: 24},
		{Name: "li", Age: 23},
		{Name: "wang", Age: 22},
	}
	for _, stu := range stus {
		m[stu.Name] = &stu
	}
	return m
}

func main() {
	students := paseStudent()

	fmt.Println("-------------result---------------")
	for k, v := range students {
		fmt.Printf("key=%s,value=%v \n", k, v.Age)
	}
}
