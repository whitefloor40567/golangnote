// Package basicexample 每個.go檔都會有個package name，name是該資料夾的名稱
package basicexample

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
)

var name string

var wr io.Reader

// AboutPrintln ...
func AboutPrintln() {
	r := bytes.NewReader([]byte("123"))

	if _, err := io.Copy(os.Stdout, r); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Hello Golang")
	log.Println("Hello Golang")
}
