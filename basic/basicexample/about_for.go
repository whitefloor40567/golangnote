package basicexample

import "log"

// AboutForFirst //Go沒有While，此種用法就是在Go中使用while的方法
func AboutForFirst() {
	num := 0

	//當num<10就會執行方法
	for num < 10 {
		log.Println(num)
		num++
	}
}

// AboutForSecond ...
func AboutForSecond() {

	//i一開始=0，只要i<5就會執行迴圈內的方法，然後i++
	//初始值;條件式;運算式
	for i := 0; i < 5; i++ {
		log.Println(i)
		//defer：在該f內有所方法執行結束後執行
		defer log.Println("i am here")
	}

	//可以將初始值 or 運算式用其他方式表達
	//初始值外置
	k := 0
	for ; k < 5; k++ { // 注意：此種寫法條件是要有個;
		log.Println(k)
	}

	//運算式內置
	h := 0
	for h < 5 {
		log.Println(h)
		h++
	}
}

// AboutForThird range的使用方法
func AboutForThird() {
	var intArray = [5]int{5, 4, 3, 2, 1}

	//range方法會把array/map中所有的索引與值遍歷一次
	//j是array裡的索引，k代表array裡每次對應索引的值
	for j, k := range intArray {
		log.Println(j, k)
		//如果 j = 3 則迴圈繼續
		if j == 3 {
			continue
		}
		//如果j = 4 則迴圈直接中斷
		if j == 4 {
			break
		}
	}
}
