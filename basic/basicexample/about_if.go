package basicexample

import (
	"log"
	"os"
)

//宣告常數，代表這個變數只要被呼叫就只會有對應的數值
const (
	b int = 2
)

//短宣告在func外是會被編譯器報錯的，可以把remakr解除看看
// a:=2

// AboutIf func首字如果是大寫，就可以給別的package使用，也就是Public，公開的，反之如果是小寫則只能在該package使用
func AboutIf() {

	//再多值回傳的情況下，底線可以將回傳值略過不進行使用
	//golang的if可以直接進行宣告，在對宣告的變數進行判斷
	if _, err := os.Open("./test.txt"); err != nil {
		log.Println("Not have find file")
	}

	//短宣告，等同於var a int = 1、var a =1，但短宣告只能在func內使用
	a := 1

	//這裡if判斷的status意思是 status == true，兩種寫法是一樣的
	if status := a > b; status {
		log.Println("a>b")
	} else if !status {
		log.Println("a<b")
	}
}
