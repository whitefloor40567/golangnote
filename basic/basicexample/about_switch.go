package basicexample

import (
	"fmt"
	"log"
	"math/rand"
	"time"
)

// AboutSwitchFirst ...
func AboutSwitchFirst() {
	num := getContent()

	//此處可以寫成 switch num := getContent();num{} 與if可以在判斷中進行宣告是一樣的
	switch num {
	case 0:
		log.Println("A")
		//case內自帶break方法，執行完就會跳出switch
	case 1:
		log.Println("B")
	case 2:
		log.Println("C")
	case 3:
		log.Println("D")
	default: //default:當前面條件都沒有滿足時將會執行此處內包含的方法
		fmt.Println(num)
	}
}

// AboutSwitchSecond ...
func AboutSwitchSecond() {

	switch num := getContent(); num {
	case 0:
		log.Println("A")
		fallthrough //fallthrough：意思是當進入執行且執行完case 0 時，會接著執行下一個case的方法
	case 1:
		log.Println("B")
		fallthrough
	case 2:
		log.Println("C")
		fallthrough
	case 3, 4: //case可以有多個表達式，此處意思為num = 3 or 4 就會進入
		log.Println("D")
		fallthrough
	default:
		fmt.Println(num)
		//switch的最後一個case不能放入fallthrough
	}
}

// AboutSwitchThird ...
func AboutSwitchThird() {

	num := getContent()

	// switch也可以不放入任何的type，以條件進行判斷
	switch {
	case num <= 2:
		log.Println("A")
	case num > 2:
		log.Println("B")
	default: //default:當前面條件都沒有滿足時將會執行此處內包含的方法
		fmt.Println(num)
	}
}

// 注意：還有一種針對interface使用判斷interface所回傳的type，但較為深入，放在interface後再進行解說

// 小寫開頭的func只能在該package呼叫，好處是其他package無法使用，能夠隱藏細節進行封裝
func getContent() int {
	//rand的使用方法請看官方文件，此處重點為switch
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(4)

	// 如果宣告的回傳值為匿名，那回傳時一定要賦予變數名稱
	return num
}
