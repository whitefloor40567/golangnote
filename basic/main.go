//一定要有一個main package提供程式第一次進入的點
package main

import (
	"fmt"
	"log"
	"os"
)

//程式進入點，固定叫main
func main() {
	//要用到對應的func請把remark解除

	//使用其他package時的表現方式(package.func())
	//hello golang
	// example.AboutPrintln()

	//if
	// example.AboutIf()

	//switch
	// example.AboutSwitchFirst()
	// example.AboutSwitchSecond()
	// example.AboutSwitchThird()

	//for
	// example.AboutForFirst()
	// example.AboutForSecond()
	// example.AboutForThird()

	a := 1
	n, _ := fmt.Fprintf(os.Stdout, "Test's is %v\n", a)

	log.Println(n)
}
