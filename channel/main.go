package main

// func main() {
// 	go func() {
// 		log.Println("123")
// 	}()
// 	//沒有暫停1秒鐘讓另一個routing準備好，有可能程式會直接結束而不會輸出任何東西
// 	time.Sleep(1 * time.Second)
// }

// func main() {
// 	ch := make(chan bool)
// 	go func() {
// 		ch <- true
// 	}()
// 	// 雖然沒有time.Sleep等待goroutine執行
// 	// 但最後有一個<-ch一直讀取channel
// 	// 所以可以看到Print true
// 	log.Println(<-ch)
// }

// func main() {
// 	ch := make(chan int, 10)
// 	go func() {
// 		for i := 0; i < 10; i++ {
// 			ch <- i
// 		}
// 		close(ch)
// 	}()

// 	for value := range ch {
// 		log.Println(value)
// 	}
// }

// func main() {
// 	ch := make(chan int, 10) // 可以加上cap，一樣可以正常跑程式，而且可以看出UnBuffered & Buffered的差異
// 	go func() {
// 		for i := 0; i < 10; i++ {
// 			log.Println("Send value", i)
// 			ch <- i
// 		}
// 		close(ch) // close channel很重要，沒有閉關的話，另一端一直在讀取，而沒有資料寫入，就會產生deadlock
// 	}()

// 	for {
// 		val, ok := <-ch
// 		if ok {
// 			log.Println("Receive value", val)
// 		} else {
// 			break
// 		}
// 	}
// }

// func main() {
// 	str := "stop"
// 	c := make(chan int)
// 	s := make(chan string)

// 	for i := 0; i < 50; i++ {
// 		go func(x int) {
// 			if x == 20 {
// 				log.Println("goroutine will stop")
// 				s <- str
// 			} else {
// 				log.Println("Send value", x)
// 				c <- x
// 			}
// 		}(i)
// 	}

// LOOP:
// 	for {
// 		select {
// 		case v, ok := <-c:
// 			if ok {
// 				log.Println("Select value", v)
// 			}
// 		case str := <-s:
// 			log.Println(str)
// 			break LOOP
// 		default:
// 			log.Println("wait")
// 		}
// 	}
// }
