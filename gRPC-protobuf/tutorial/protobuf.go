package tutorial

import (
	"log"

	"google.golang.org/protobuf/proto"
)

// ProtoExample ...
func ProtoExample() {
	// 你一定很好奇這個struct在哪，在tutorial.pb.go
	p := HelloRequest{
		Name: "Alice",
	}

	// Writing a Message
	bp, err := proto.Marshal(&p)
	if err != nil {
		log.Fatal(err)
	}

	// Reading a Message
	var data HelloRequest
	if err = proto.Unmarshal(bp, &data); err != nil {
		log.Fatal(err)
	}
	log.Println(&data)
}
