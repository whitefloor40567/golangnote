package tutorial

import (
	"context"
	"log"
	"net"

	grpc "google.golang.org/grpc"
)

type service struct {
	UnimplementedGreeterServer
}

func (s *service) SayHello(ctx context.Context, in *HelloRequest) (*HelloReply, error) {
	log.Printf("Received: %v", in.GetName())
	return &HelloReply{Message: "Hello, " + in.GetName()}, nil
}

// GRPCServer ...
func GRPCServer() {
	addr := "127.0.0.1:8080"
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Println("Server listening on", addr)
	gRPCServer := grpc.NewServer()
	RegisterGreeterServer(gRPCServer, &service{})
	if err := gRPCServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
