package interfacebasic

import "log"

// InterfaceGuide interface可以是任意型態
func InterfaceGuide(ent interface{}) {
	//switch的特殊用法，判斷型態
	switch ent.(type) {
	case int:
		log.Println("this type is int")
	case string:
		log.Println("this type is string")
	}
}
