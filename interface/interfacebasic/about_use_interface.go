package interfacebasic

import "log"

//interface使用方法
// 1.宣吿interface與實現該interface的方法，以及實現interface方法的傳入值&回傳值
// 2.interface的實現對象是struct，所以需要宣告struct
// 3.以struct實作interface方法
// 4.將struct實例傳入interface內，即可運作

// SearchSystem 宣告interface
type SearchSystem interface {
	EnterISBN(ISBN int)
	EnterBookName(bookname string)
}

// Book 宣告struct
type Book struct {
	BookISBN int
	BookName string
}

//實作interface的方法如下

// EnterISBN .. 輸入ISBN碼
func (b *Book) EnterISBN(ISBNnum int) {
	if b.BookISBN == ISBNnum {
		log.Println("BookISBN is ", b.BookISBN)
	} else {
		log.Println("Enter BookISBN Error")
	}
}

// EnterBookName ... 輸入書名
func (b *Book) EnterBookName(bookname string) {
	if b.BookName == bookname {
		log.Println("BookName is ", b.BookName)
	} else {
		log.Println("Enter BookName Error")
	}
}

// Run ...
func Run(s SearchSystem) {
	ISBN := 40666888
	Bookname := "War"

	s.EnterISBN(ISBN)
	s.EnterBookName(Bookname)
}
