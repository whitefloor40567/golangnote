package main

import "golangnote/interface/interfacebasic"

func main() {
	//interface guide
	// num := 100
	// str := "test"
	// interfacebasic.InterfaceGuide(num)
	// interfacebasic.InterfaceGuide(str)

	bookInfo := interfacebasic.Book{
		BookISBN: 40666888,
		BookName: "War",
	}
	interfacebasic.Run(&bookInfo)
}
