package main

func main() {
	data := PersonalData{
		Name: "Jack",
		Age:  25,
	}

	name := "Leo"
	age := 30

	//method 使用
	// (struct).func(){}
	data.ChangeDataFirst(name, age)
	data.PrintData()

	data.ChangeDataSecond(name, age)
	data.PrintData()
}
