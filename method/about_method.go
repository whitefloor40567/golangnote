package main

import "log"

// PersonalData ...
type PersonalData struct {
	Name string
	Age  int
}

//method 宣告
// (struct).func(){}

//如果方法裡的struct沒有用point，將只能讀取struct裡的數值，而不能做修改
//且如果struct裡變數眾多，用point將可以增加程式效率
//所以建議用method時的統一寫法都用point傳入
//也可以進一步統一程式碼風格，增加維護的方便性

// ChangeDataFirst ... no point
func (p PersonalData) ChangeDataFirst(name string, age int) {
	log.Println("---Start ChangeDataFirst---")

	p.Name = name
	p.Age = age
}

// ChangeDataSecond ... Use point
func (p *PersonalData) ChangeDataSecond(name string, age int) {
	log.Println("---Start ChangeDataSecond---")

	p.Name = name
	p.Age = age
}

// PrintData ...
func (p PersonalData) PrintData() {
	log.Println("---PrintData---")

	log.Println(p.Name)
	log.Println(p.Age)
}
